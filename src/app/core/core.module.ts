import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastNotifyModule } from './toast-notify/toast-notify.module';
import { InterceptorsModule } from './interceptors/interceptors.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ToastNotifyModule,
    InterceptorsModule
  ],
  exports: [
    ToastNotifyModule,
  ]
})
export class CoreModule {}
