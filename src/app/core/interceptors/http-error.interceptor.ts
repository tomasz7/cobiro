import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ToastNotifyService} from './../toast-notify/_service/toast-notify.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private toastNotifyService: ToastNotifyService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMsg;
        if (error.error instanceof ErrorEvent) {
          this.toastNotifyService.showError(`Error: ${error.error.message}`);
          errorMsg = `Error: ${error.error.message}`;
        } else {
          this.toastNotifyService.showError(`Error Code: ${error.status},  Message: ${error.message}`);
          errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;
        }
        return throwError(errorMsg);
      })
    );
  }
}
