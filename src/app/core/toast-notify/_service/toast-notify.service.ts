import {Injectable} from '@angular/core';
import {MessageService} from 'primeng/api';

export enum TYPE_MESSAGE {
  ERROR = 'error',
  SUCCESS = 'success',
  WARN = 'warn',
  INFO = 'info'
}

@Injectable({
  providedIn: 'root'
})
export class ToastNotifyService {

  constructor(private messageService: MessageService) {}

  public showError(error: string): void {
    this.messageService.add({severity: TYPE_MESSAGE.ERROR, summary: TYPE_MESSAGE.ERROR, detail: error});
  }

  public showSuccess(): void {
    this.messageService.add({severity: TYPE_MESSAGE.SUCCESS, summary: TYPE_MESSAGE.SUCCESS, detail: 'Via MessageService'});
  }

  public showWarn(): void {
    this.messageService.add({severity: TYPE_MESSAGE.WARN, summary: TYPE_MESSAGE.WARN, detail: 'Via MessageService'});
  }

  public showInfo(): void {
    this.messageService.add({severity: TYPE_MESSAGE.INFO, summary: TYPE_MESSAGE.INFO, detail: 'Via MessageService'});
  }
}
