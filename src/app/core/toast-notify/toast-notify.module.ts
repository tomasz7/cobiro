import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToastNotifyComponent} from './toast-notify/toast-notify.component';
import {ToastNotifyService} from './_service/toast-notify.service';
import {MessageService} from 'primeng/api';
import {ToastModule} from 'primeng/toast';

@NgModule({
  declarations: [
    ToastNotifyComponent
  ],
  imports: [
    CommonModule,
    ToastModule
  ],
  exports: [
    ToastNotifyComponent
  ],
  providers: [
    ToastNotifyService,
    MessageService
  ]
})
export class ToastNotifyModule {}
