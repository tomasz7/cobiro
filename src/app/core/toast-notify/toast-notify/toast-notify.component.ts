import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-toast-notify',
  templateUrl: './toast-notify.component.html',
  styleUrls: ['./toast-notify.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToastNotifyComponent {

  constructor() { }

}
