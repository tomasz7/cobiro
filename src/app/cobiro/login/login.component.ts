import {Component, OnInit} from '@angular/core';
import {FormService} from './_service/form.service';
import {LoginService} from './_service/login.service';
import {FormGroup} from '@angular/forms';
import {LoginAttributes} from './_interface/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(
    private formService: FormService,
    private loginService: LoginService,
  ) { }

  ngOnInit(): void {
    this.loginForm = this.formService.buildLoginForm();
  }

  public sendForm(form: LoginAttributes): void {
    this.loginService.loginUser(form).subscribe();
  }

}
