export interface Login {
  data: LoginData;
}

export interface LoginData {
  type: string;
  attributes: LoginAttributes;
}

export interface LoginAttributes {
  email: string;
  password: string;
}
