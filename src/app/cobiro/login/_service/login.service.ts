import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from '@env';
import {Login, LoginAttributes} from '../_interface/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {}

  public loginUser(form: LoginAttributes): Observable<any> {
    return this.http.post(`${environment.url}/login`, {data: {
        type: 'login',
        attributes: form
      }} as Login
    );
  }
}
