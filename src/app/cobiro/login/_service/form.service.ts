import { Injectable } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';

export const emailValidator = (): ValidatorFn => {
  return (control: AbstractControl): ValidationErrors | null => {
    const validEmailRegex = /.+@.+\..+/;
    if (!validEmailRegex.test(control.value)) {
      return { invalidEmail: true };
    }
    return null;
  };
};

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(private formBuilder: FormBuilder) { }

  public buildLoginForm(): FormGroup {
    return this.formBuilder.group({
      email: ['', [Validators.required, emailValidator()]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
}
