import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {LoginAttributes} from '../../_interface/login';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormComponent {
  @Output() public emitLoginForm: EventEmitter<LoginAttributes> = new EventEmitter<LoginAttributes>();
  @Input() public loginForm: FormGroup;

  constructor() { }

  public emitSendForm(): void {
    this.emitLoginForm.emit(this.loginForm.value);
  }

}
